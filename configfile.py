#!/usr/bin/env python
# -*- mode: Python; coding: utf-8 -*-

from qcontrol3.tools import qc3config

class ConfigFile(qc3config.ConfigFile):

    _filename = 'script_viewer'
    _content = [
        ('app',
         'Describes the application',
         [
             (
                 'name',
                 'script_viewer',
                 'The application name',
             ),
             (
                 'version',
                 '0.1.0',
                 'The application version',
             ),
             (
                 'description',
                 'Script viewer',
                 'The application description',
             ),
             (
                 'authors',
                 [],
                 'The application authors',
             ),
             (
                 'emails',
                 [],
                 'The application authors\' email addresses',
             ),
             (
                 'organization_name',
                 'srlab',
                 'Organization name',
             ),
             (
                 'organization_domain',
                 'srlab.net',
                 'Organization domain',
             ),
         ]),
        ('gui',
         'Configures the gui',
         [
             (
                 'geometry',
                 [20, 30, 1024, 768],
                 'Window geometry as [X, Y, WIDTH, HEIGHT]',
             ),
             (
                 'main_window_title',
                 'Script viewer',
                 'Window title',
             ),
             (
                 'main_page_splitter_sizes',
                 [768, 0],
                 'Main page splitter heights'
             ),
             (
                 'userwidget_splitter_sizes',
                 [100, 668],
                 'User widget splitter heights'
             ),
             (
                'channels_data',
                {},
                'Channels data'
             ),
         ]),
    ]

# config.py ends here
