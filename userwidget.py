#!/usr/bin/env python
# -*- mode: Python; coding: utf-8 -*-

from qcontrol3.gui.qt import qt
from qcontrol3.gui.qt.widget.matplotlib import plotcanvas
from qcontrol3.gui.qt.app import page
from qcontrol3.gui.qt.layout import layout
from PyQt5 import QtChart
import numpy as np

class MyChartView(QtChart.QChartView):
    def __init__(self, callback, *args, **kwargs):
        chart = QtChart.QChart()
        chart.legend().hide()
        chart.layout().setContentsMargins(0, 0, 0, 1);
        chart.setBackgroundRoundness(0);

        super().__init__(chart, *args, **kwargs)
        self.callback = callback
        self.setRubberBand(QtChart.QChartView.HorizontalRubberBand);
        # self.setRenderHint(qt.QtGui.QPainter.Antialiasing)

    def mouseReleaseEvent(self, *args, **kwargs):
        chart = self.chart()
        ax = chart.axisX()
        oldmin = ax.min()
        oldmax = ax.max()
        super().mouseReleaseEvent(*args, **kwargs)
        print("scale")
        if (ax.min() >= oldmin) and (ax.max() <= oldmax):
            self.callback(self, ax.min(), ax.max())
        else:
            self.callback(self, None, None)

    def wheelEvent(self, ev, *args, **kwargs):
        if ev.type() == qt.QtCore.QEvent.Wheel:
            ev.ignore()

    def set_title(self, title):
        self.title = title
        self.chart().setTitle(title)

    def clear(self):
        self.chart().removeAllSeries()

    def update_series(self, t, v):
        self.is_digital = True
        self.chart().removeAllSeries()
        self.series = QtChart.QLineSeries()
        self.series.setPen(self.get_pen())
        self.t = t
        self.v = v
        for i in range(len(t)):
            self.series.append(t[i],v[i])
            if v[i] != 0 and v[i] != 1:
                self.is_digital = False
            if i<len(t)-1:
                self.series.append(t[i+1],v[i])
        self.chart().addSeries(self.series)
        self.make_axes(values=v)
        self.repaint()

    def make_axes(self, values=None):
        self.chart().createDefaultAxes()
        ax = self.chart().axisY()
        if self.is_digital:
            ax.setTickCount(2)
            ax.setMinorTickCount(0)
            ax.setLabelFormat("%.0f")
            ax.setRange(-0.01, 1.01)
        else:
            if values is not None:
                vmin = min(values)
                vmax = max(values)
                delta = vmax-vmin
                ax.setRange(vmin-0.01*delta, vmax+0.01*delta)
        # ax = self.chart().axisX()
        # ax.applyNiceNumbers()

    def get_pen(self):
        channel = self.title
        color = qt.QtGui.QColor(*bytes.fromhex("4daf4a"))
        if "RED" in channel:
            color = qt.QtGui.QColor(*bytes.fromhex("e41a1c"))
        if "REPUMP" in channel:
            color = qt.QtGui.QColor(*bytes.fromhex("fb6a4a"))
        if "BLUE" in channel or "PROBE" in channel or "ZEEMAN" in channel:
            color = qt.QtGui.QColor(*bytes.fromhex("377eb8"))
        if "COIL" in channel:
            color = qt.QtGui.QColor(*bytes.fromhex("ff7f00"))
        pen = qt.QtGui.QPen(color)
        pen.setWidth(2)
        return pen

    def zoom_to(self, xmin, xmax):
        c = self.chart()
        ax = c.axisX()
        ax.setMin(xmin)
        ax.setMax(xmax)

class PlotListWidget(page.MainWidgetPage):
    def __init__(self, parent, model):
        super().__init__(parent, model)
        # self._model.subscriber.newFileEvent.connect(self._on_new_file)
        self._model.subscriber.dataUpdateEvent.connect(self._on_new_data)
        self.xmin = 0
        self.xmax = 1

    def _on_new_file(self, file):
        print("PlotListWidget: new file")
        print(file)

    def _on_new_data(self, data):
        print("PlotListWidget: new data")
        channels = [ch for ch in sorted(data.keys()) if data[ch]["data"] is not None and data[ch]["active"]]
        n = len(channels)

        # ax0=None
        for chart in self.charts:
            chart.hide()

        n_analog = 0
        n_digital = 0
        for i, ch in enumerate(channels):
            if(i < len(self.charts)):
                chart = self.charts[i]
                chart.show()
            else:
                chart = self.add_chart()
            chart.set_title(ch)
            chart.clear()

            t = data[ch]["data"]["t"]
            v = data[ch]["data"]["v"]
            self.xmin = min(t)
            self.xmax = max(t)

            chart.update_series(t, v)
            if chart.is_digital:
                n_digital+=1
            else:
                n_analog+=1

        h_analog=200
        h_digital=200
        desired_height = n_digital*h_digital+n_analog*h_analog
        self.box.setMinimumHeight(desired_height)
        boxheight = self.box.height()
        if desired_height > 0:
            scale_factor = boxheight / desired_height
        else:
            scale_factor = 1
        for chart in self.charts:
            if chart.is_digital:
                chart.setMaximumHeight(h_digital*scale_factor)
            else:
                chart.setMaximumHeight(h_analog*scale_factor)

    def _on_zoom(self, chartView, xmin=None, xmax=None):
        print("_on_zoom")
        if xmin is None:
            xmin = self.xmin
        if xmax is None:
            xmax = self.xmax
        xmin = max(xmin, self.xmin)
        xmax = min(xmax, self.xmax)
        for chart in self.charts:
            chart.zoom_to(xmin, xmax)

    def add_chart(self):
        chartView = MyChartView(self._on_zoom)
        self.layout.addWidget(chartView)
        self.charts.append(chartView)
        return chartView

    def _initialize_ui(self):
        self.box = qt.QtWidgets.QWidget()
        self.layout = layout.VBoxLayout(spacing=0)
        self.charts = []
        self.box.setLayout(self.layout)

        scroll = qt.QtWidgets.QScrollArea()
        scroll.setWidget(self.box)
        scroll.setWidgetResizable(True)

        grid = layout.VBoxLayout()
        grid.addWidget(scroll)
        self.setLayout(grid)

class ChannelsWidget(page.MainWidgetPage):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._model.subscriber.dataUpdateEvent.connect(self._on_new_data)

    def _update_channel_callback(self, ch, checkbox):
        def fn():
            self._model.update_channel(ch, checkbox.isChecked())
        return fn

    def checkall_callback(self):
        checkall = self.checkall.isChecked()
        self._model.update_all_channels(checkall)

    def _initialize_ui(self):
        box = qt.QtWidgets.QWidget()
        box_layout = layout.VBoxLayout(margins=10)

        self._channels = {}
        channels = self._model.get_channels()
        for ch in sorted(channels.keys()):
            checkbox = qt.QtWidgets.QCheckBox(ch)
            checkbox.setChecked(channels[ch]["active"])
            self._channels[ch] = checkbox
            checkbox.stateChanged.connect(self._update_channel_callback(ch, checkbox))
            box_layout.addWidget(checkbox)
        empty = qt.QtWidgets.QWidget();
        empty.setSizePolicy(qt.QtWidgets.QSizePolicy.Expanding,
                            qt.QtWidgets.QSizePolicy.Preferred)
        box_layout.addWidget(empty)
        box.setLayout(box_layout)

        scroll = qt.QtWidgets.QScrollArea()
        scroll.setStyleSheet("border:1px solid transparent;");
        scroll.setWidget(box)
        scroll.setWidgetResizable(True)

        grid = layout.VBoxLayout()
        grid.addWidget(qt.QtWidgets.QLabel('Channels:'))
        
        # button = qt.QtWidgets.QPushButton('Emit!')
        # button.clicked.connect(self._on_btn_click)
        # grid.addWidget(button)

        self.checkall = qt.QtWidgets.QCheckBox("Check/uncheck all")
        self.checkall.stateChanged.connect(self.checkall_callback)
        grid.addWidget(self.checkall)

        grid.addWidget(scroll)
        self.setLayout(grid)

    def _on_new_data(self, data):
        for ch in self._channels:
            checkbox = self._channels[ch]
            checkbox.setChecked(data[ch]["active"])
            if ch in data and data[ch]["data"] is not None:
                checkbox.show()
            else:
                checkbox.hide()

    def _on_btn_click(self):
        # data = self._model.get_data()
        self._model.subscriber.newFileEvent.emit("examples/coils_example.pys")
        # self._model.subscriber.newFileEvent.emit("red_mot_180228.pys")
        # self._model.subscriber.dataUpdateEvent.emit(data)

class UserWidget(page.HorizontalSplitPage):
    CONFIG_SPLITTER_SIZES = ('gui', 'userwidget_splitter_sizes')

    def _create_right_widget(self):
        self._plot = PlotListWidget(self, self._model)
        return self._plot

    def _create_left_widget(self):
        self._channels = ChannelsWidget(self, self._model)
        return self._channels

# userwidget.py ends here
