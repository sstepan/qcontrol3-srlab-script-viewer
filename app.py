#!/usr/bin/env python3
# -*- mode: Python; coding: utf-8 -*-

from qcontrol3.gui.qt.app import app
from qcontrol3.gui.qt.app import mainwindow

import configfile
import model
import mainwidget

if __name__ == "__main__":

    config_file = configfile.ConfigFile()
    model = model.Model(config_file)
    app = app.Qcontrol3App(model, mainwindow.MainWindow, mainwidget.MainWidget)
    app.run()

# app.py ends here
