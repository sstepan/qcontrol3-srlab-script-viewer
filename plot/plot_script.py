#!/usr/bin/env python3
# -*-mode: Python; coding: utf-8 -*-
import sys, os
from qcontrol3.tools import units as u
from qcontrol3.server import pysmanager
import matplotlib.pyplot as plt
import numpy as np

# script_root = os.path.dirname(os.path.realpath(__file__))
# script_root = '/Users/ss/Dropbox/Strontium/Projects/TimingSystem/control/scripts/'
script_root = '/home/srlab/strontium/python/scripts/'
sys.path.append(script_root)
from config import channels
import collections

class DummyChannel(object):
    def __init__(self, name, default=0, unit=1, **kwargs):
        self.name = name
        self.times = [0*u.s]
        self.values = [default]
        self.unit=unit

    def add(self, time, value):
        tmax = self.times[-1]
        if isinstance(time, collections.Iterable):
            if len(time) != len(value):
                raise RuntimeError("length of times and values array are different")
            if tmax >= min(time):
                raise RuntimeError("{} time collision at time {}".format(self.name, min(time)))
            for t in time:
                self.times.append(t)
            for v in value:
                self.values.append(v)
        else:
            if tmax >= time:
                raise RuntimeError("{} time collision at time {}".format(self.name, time))
            self.times.append(time)
            self.values.append(value)

    def get_value_at_time(self, t):
        ix = 0
        for i, tx in enumerate(self.times):
            if tx < t:
                ix = i
        return self.values[ix]

    def wait(self, t):
        return t

class DummyBoolChannel(DummyChannel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.unit = 1

class DummyVoltChannel(DummyChannel):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.unit = u.V

class TimingSystemDummy(object):
    def __init__(self, *args, **kwargs):
        self.channels = {}
        self.script_root = script_root
        if self.script_root[-1]!='/':
            self.script_root += '/'
        print(self.script_root)

    def get_user_channels_by_alias(self):
        return self.channels

    def get_node(self, node):
        return self

    @property
    def user_channel(self):
        pass

    @user_channel.setter
    def user_channel(self, ch):
        self.add_user_channel(None, ch)

    def add_user_channel(self, real_name, channel):
        name = channel.name
        default = channel._event_list.default_value
        if hasattr(channel, "unit"):
            unit = channel.unit
        else:
            unit = 1
        self.channels[name] = DummyChannel(name, default, unit)

    def reset(self):
        self.channels = {}

tsys = TimingSystemDummy()

# Create a global importer for pys files and add it to sys.meta_path
for c in sys.meta_path:
    if c.__class__.__name__ == pysmanager.PysImporter.__name__:
        print(('PysImporter class from {} found in sys.meta_path. ' +
                     'Replacing with my {} global copy.')
                    .format(c.__module__, __name__))
        sys.meta_path.remove(c)
importer = pysmanager.PysImporter()
importer.set_tsys(tsys)
sys.meta_path.append(importer)

from config import objects
import config.srlab as srlab

channels.channel_setup(tsys)

def get_plot_data(fname):
    tsys.reset()
    channels.channel_setup(tsys)
    s = importer.import_script(fname)

    ts = s.timing_script
    ts.main()

    i = 0
    tmax = 0 * u.s
    for chname in tsys.channels:
        ch = tsys.channels[chname]
        tmax = max(tmax, max(ch.times))

    # channels to display
    ch_names = sorted([ch for ch in tsys.channels if len(tsys.channels[ch].times)>1])
    num_ch = len(ch_names)
    
    d = {}
    for i, chname in enumerate(ch_names):
        ch = tsys.channels[chname]

        times = [t / u.ms for t in ch.times]
        values = [v / ch.unit for v in ch.values]
        times.append(tmax / u.ms)
        values.append(ch.get_value_at_time(tmax) / ch.unit)
        values = np.array(values)
        # times_arr.append(times)
        # values_arr.append(values)
        d[chname] = {"t": times, "v": values}
    return d

def get_channel_names():
    return list(tsys.channels.keys())

def plot_script(fig, fname):

    channels.channel_setup(tsys)
    s = importer.import_script(fname)

    ts = s.timing_script
    ts.main()

    i = 0
    tmax = 0 * u.s
    for chname in tsys.channels:
        ch = tsys.channels[chname]
        tmax = max(tmax, max(ch.times))

    # channels to display
    ch_names = [ch for ch in tsys.channels if len(tsys.channels[ch].times)>1]
    num_ch = len(ch_names)
    print(num_ch)

    ax0 = None
    for i, chname in enumerate(sorted(ch_names)):
        if ax0 is None:
            ax = fig.add_subplot(num_ch,1,num_ch-i, frameon=False)
            ax0 = ax
            ax.get_yaxis().set_ticks([])
        else:
            ax = fig.add_subplot(num_ch,1,num_ch-i, sharex=ax0, frameon=False)
            ax.axis('off')
        ch = tsys.channels[chname]
        print(ch.name)
        times = [t / u.ms for t in ch.times]
        values = [v / ch.unit for v in ch.values]
        times.append(tmax / u.ms)
        values.append(ch.get_value_at_time(tmax) / ch.unit)
        values = np.array(values)
        # print(times, values)
        ax.step(times, values, where='post', label=ch.name)
    # ax.legend(bbox_to_anchor=(1.05, 1))
    # ax.set_title(fname)
    # fig.subplots_adjust(right=0.7)
    return fig

if __name__ == '__main__':
    fig = plt.figure(figsize=[10,5])
    filename = sys.argv[-1]
    plot_script(fig, filename)
    plt.show()