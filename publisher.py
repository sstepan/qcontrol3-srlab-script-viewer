from qcontrol3.tools import qpyro
from qcontrol3.tools.EventService4 import Clients
import sys

qpyro.configurepyro()

if __name__ == '__main__':
    fname = sys.argv[-1]
    p = Clients.Publisher()
    print(fname)
    p.publish('PLOT_NEW_SCRIPT', fname)
