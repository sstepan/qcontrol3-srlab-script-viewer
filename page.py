#!/usr/bin/env python
# -*- mode: Python; coding: utf-8 -*-

from qcontrol3.gui.qt import qt
from qcontrol3.gui.qt.app import page
from qcontrol3.gui.qt.widget.logging import logtable
from qcontrol3.gui.qt.layout import layout

import userwidget

class MainPage(page.MainPage):
    NAME = "Script viewer"

    def _create_top_widget(self):
        return userwidget.UserWidget(self, self._model)

    def _create_bottom_widget(self):
        return page.DebugPage(self, self._model)

class PreferencesPage(page.MainWidgetPage):
    NAME = "Preferences"

    def _initialize_ui(self):
        pass

    def _save_geometry(self):
        pass

# class DebugPage(page.DebugPage):

#     def _initialize_ui(self):
#         storage = self._model._log_storage
#         formatter = logtable.LogFormatterProcessed()
#         logfilter = logtable.LogFilter()
#         table_model = logtable.LogTableModel(storage,
#                                              formatter,
#                                              logfilter,
#                                              storage.max_records())
#         w = logtable.LogTable(self, table_model)
#         w.doubleClicked.connect(self._on_click)
#         grid = layout.VBoxLayout()
#         grid.addWidget(w)

#         self.setLayout(grid)

#     def _on_click(self, item):
#         message = self._model._log_storage.get(item.row())["msg"]
#         qt.QtWidgets.QMessageBox.about(self, "Log message",message)

# page.py ends here
