#!/usr/bin/env python
# -*- mode: Python; coding: utf-8 -*-

from qcontrol3.gui.qt.app import model

import subscriber
import numpy as np
from datetime import datetime
from plot import plot_script
import os, traceback

class Model(model.Model):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.subscriber = subscriber.EventServerSubscriberThread()
        self.subscriber.newFileEvent.connect(self._on_new_file)

        self.subscriber.dataUpdateEvent.connect(self._on_new_data)

        self._threads = [
            self.subscriber
        ]
        channels = plot_script.get_channel_names()
        self._channels = {}
        channels_config = self._config_file.get('gui', 'channels_data')
        for ch in channels:
            active = True
            if ch in channels_config:
                active = channels_config[ch]["active"]
            self._channels[ch] = { "data": None, "active": active }
        self.get_data()

    def get_channels(self):
        return self._channels

    def update_channel(self, ch, val):
        if self._channels[ch]["active"] != val:
            self._channels[ch]["active"] = val
            self.subscriber.dataUpdateEvent.emit(self._channels)
            self.save_channels()

    def update_all_channels(self, val):
        for ch in self._channels:
            if self._channels[ch]["data"] is not None:
                self._channels[ch]["active"] = val
        self.subscriber.dataUpdateEvent.emit(self._channels)
        self.save_channels()

    def save_channels(self):
        channels_config = {}
        for ch in self._channels:
            channels_config[ch] = { "active": self._channels[ch]["active"] }
        self._config_file.set('gui', 'channels_data', channels_config)

    def get_data(self):
        return self._channels

    def get_default_data(self):
        return np.cos(np.linspace(0, 2*np.pi, 20))

    def log_exception(self, e):
        self.log(traceback.format_exc(), levelname='CRITICAL')
        print(e)
        print(traceback.format_exc())

    def log(self, message, levelname='DEBUG'):
        self._log_storage.append({
            'levelname': levelname, 
            'asctime': datetime.now().strftime("%H:%M:%S"), 
            'pathname': '', 
            'funcName': '', 
            'lineno': 0,
            'process': '', 
            'threadName': '', 
            'msg': message
        })

    def clear_channels_data(self):
        for ch in self._channels:
            self._channels[ch]["data"] = None

    def _on_new_file(self, file):
        self.log(file, levelname='INFO')
        try:
            data = plot_script.get_plot_data(file)
            self.clear_channels_data()
            for ch in data:
                if ch in self._channels:
                    self._channels[ch]["data"] = data[ch]
                else:
                    self._channels[ch] = { "active": True, "data": data[ch]}
            self.subscriber.dataUpdateEvent.emit(self._channels)
            self.save_channels()
        except Exception as e:
            self.log_exception(e)
            self.clear_channels_data()
            self.subscriber.dataUpdateEvent.emit(self._channels)

    def _on_new_data(self, obj):
        pass
        # self.log('New data event!')


# model.py ends here
