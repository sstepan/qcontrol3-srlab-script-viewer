#!/usr/bin/env python
# -*- mode: Python; coding: utf-8 -*-

from qcontrol3.gui.qt import qt
from qcontrol3.gui.qt.app import mainwidget
from qcontrol3.gui.qt.app import page as qcpage

import page

class MainWidget(mainwidget.MainWidget):

    def _create_pages(self):
        return [
            page.MainPage(self, self._model),
            qcpage.DebugPage(self, self._model),
            page.PreferencesPage(self, self._model),
        ]

# mainwidget.py ends here
